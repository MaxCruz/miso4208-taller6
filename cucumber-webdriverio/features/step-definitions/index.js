var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if(browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  When('I open the login screen', () => {
    browser.waitForVisible('button=Ingresar', 5000);
    browser.click('button=Ingresar');
  });

  When('I fill a wrong email and password', () => {
    var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys('wrongemail@example.com');

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys('123467891')
  });

  When('I fill a right email and password', () => {
    var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys('mr.cruz@uniandes.edu.co');

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys('l0535miso')
  });

  When(/^I fill incomplete the register form with (.*), (.*), (.*), (.*) and (.*)$/ , (correo1, password2, nombre3, apellido4, acepta5) => {
    var cajaLogIn = browser.element('.cajaSignUp');
    var correo = cajaSignUp.element('input[name="correo"]');
    correo.click();
    correo.keys(correo1);
    var password = cajaSignUp.element('input[name="password"]');
    password.click();
    password.keys(password2);
    var nombre = cajaSignUp.element('input[name="nombre"]');
    nombre.click();
    nombre.keys(nombre3);
    var apellido = cajaSignUp.element('input[name="apellido"]');
    apellido.click();
    apellido.keys(apellido4);
    var terminos = cajaSignUp.element('input[name="acepta"]');
    if (acepta5 == "Si") {
      terminos.click();
    }
  });

  When(/^I fill the register form with an existing (.*)$/, (user) => {
    var cajaLogIn = browser.element('.cajaSignUp');
    var correo = cajaSignUp.element('input[name="correo"]');
    correo.click();
    correo.keys(user);
    var password = cajaSignUp.element('input[name="password"]');
    password.click();
    password.keys("Fooo");
    var nombre = cajaSignUp.element('input[name="nombre"]');
    nombre.click();
    nombre.keys("Fooo");
    var apellido = cajaSignUp.element('input[name="apellido"]');
    apellido.click();
    apellido.keys("Fooo");
    var terminos = cajaSignUp.element('input[name="acepta"]');
    terminos.click();
  });

  When('I fill the register form for a new user', () => {
    var cajaLogIn = browser.element('.cajaSignUp');
    var correo = cajaSignUp.element('input[name="correo"]');
    correo.click();
    var randomN = Math.floor((Math.random() * 999) + 1).toString();
    correo.keys("foo" + randomN + "@foo.com");
    var password = cajaSignUp.element('input[name="password"]');
    password.click();
    password.keys("123456");
    var nombre = cajaSignUp.element('input[name="nombre"]');
    nombre.click();
    nombre.keys("Fooo First Name");
    var apellido = cajaSignUp.element('input[name="apellido"]');
    apellido.click();
    apellido.keys("Fooo Last Name");
    var terminos = cajaSignUp.element('input[name="acepta"]');
    terminos.click();
  });

  When('I try to login', () => {
    var cajaLogIn = browser.element('.cajaLogIn');
    cajaLogIn.element('button=Ingresar').click();
  });

  When('I try to register', () => {
    var cajaLogIn = browser.element('.cajaSignUp');
    cajaLogIn.element('button=Registrarse').click();
  });

  Then('I expect to not be able to login', () => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
  });

  Then('I expect to be able to login', () => {
    browser.waitForVisible('#cuenta', 5000);   
  });

  When(/^I fill with (.*) and (.*)$/ , (email, password) => {
     var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys(email);

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password)
  });

  Then('I expect to see {string}', error => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    var alertText = browser.element('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(error);
  });

  Then('I expect to see an error', error => {
    browser.waitForVisible('.input-group.has-error.has-feedback', 5000);
  });

  Then('I expect to be notified that the user {string}', exist => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    var alertText = browser.element('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(exist);
  });

  Then('I expect to be registered', error => {
    browser.waitForVisible('.sweet-alert', 5000);
  });

});
